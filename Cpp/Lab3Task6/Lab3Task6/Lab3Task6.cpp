#include <iostream>

using namespace std;

struct Node {
	int value;
	Node *next;
};

class List {
	Node *head;
	Node *C;
	Node *O;
public:
	List() :head(NULL), C(NULL), O(NULL) {}
	~List();

	void add(int);
	void show();
	bool isEmpty();
	void sortChoise();
	void sortBuble();
	void sortInsertion();
};

void List::add(int val) {
	Node *temp = new Node;
	temp->value = val;
	if (isEmpty()) temp->next = NULL; else temp->next = head;
	head = temp;
}

void List::show() {
	Node * temp;
	temp = head;
	while (temp) {
		cout << temp->value << ' ';
		temp = temp->next;
	}
}

bool List::isEmpty() {
	return (head == NULL);
}

List::~List() {
	Node *t = NULL;
	while (head)
	{
		t = head->next;
		delete head;
		head = t;
	}
	if (t) delete t;
}

void List::sortChoise() {
	O = NULL;

	while (O != head)
	{
		C = head;
		Node *prevMax = NULL;
		Node *Max = head;
		while (C->next != O)
		{
			Node *t = C;
			C = C->next;
			if (C->value > Max->value) {
				prevMax = t; Max = C;
			}
		}
		if (C != Max) {
			if (prevMax) prevMax->next = Max->next; else head = Max->next;
			C->next = Max;
			Max->next = O;
			O = Max;
		}
		else  O = C;
	}
}

void List::sortBuble() {
	O = NULL;
	while (O != head) {
		Node* P = NULL;
		C = head;
		while (C->next != O) {
			Node* cn = C->next;
			if (C->value > cn->value) {
				Node *t = cn->next;
				cn->next = C;
				C->next = t;
				if (P) P->next = cn; else head = cn;
				C = cn;
			}
			P = C;
			C = C->next;

			//cout << "After iteration: ";
			//show();

			//cout << endl;
			//cout << "C=" << C->value << endl;

			//system("pause");
		}

		O = C;

		//cout << " New sequence: ";
		//show();

		//cout << endl;
		//cout << "O=" << O->value << endl;

		//system("pause");
	}
}

void List::sortInsertion()
{
	if (head == NULL) return;

	O = head; // O - последний элемент в отсортированной части списка

	while (O->next != NULL)
	{
		Node* Cprev = NULL;
		C = head;

		Node* s = O->next; // s - search for, элемент, позицию для которого мы ищем в отсортированной части списка

		cout << "---" << endl;
		cout << "O: " << O->value << ".";
		cout << " Searching position for " << s->value << '.';

		bool mustSwap = false;

		while (C != s)
		{
			mustSwap = C->value > s->value;
			if (mustSwap) break;
			Cprev = C;
			C = C->next;
		}

		if (mustSwap)
		{
			cout << " Found position before " << C->value << ".";

			if (Cprev == NULL) head = s; else Cprev->next = s;

			Node* t = s->next;
			s->next = C;
			O->next = t;

			cout << " New sequence: ";
			show();
		}
		else
		{
			O = O->next;
		}

		cout << endl;
		cout << "O=" << O->value << endl;

		system("pause");
	}
}

int main()
{
	List lst1;

	cout << "---------------------------------------" << "Choise" << "---------------------------------------" << endl;
	lst1.add(1);
	lst1.add(3);
	lst1.add(2);
	lst1.add(4);
	lst1.add(5);
	lst1.show();

	lst1.sortBuble();

	cout << "\n Sorted:" << endl;
	lst1.show();

	cout << "\n ---------------------------------------" << " Bubble" << "---------------------------------------" << endl;


	system("pause");
	return 0;
}

