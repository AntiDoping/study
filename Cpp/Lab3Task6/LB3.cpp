// LB3.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

struct Node {
	int value;
	Node *next;
};

class List {
	Node *head;
public:
	List() :head(NULL) {} 
	~List();

	void add(int);
	void show();
	bool isEmpty();
};

void List::add(int val) {
	Node *temp = new Node;                  
	temp->value = val;
	temp->next = NULL;
}

void List::show() {
	Node * temp = head;
	while (temp) {
		cout << temp->value << ' ';
		temp = temp->next;
	}
}

bool List::isEmpty() {
	return (head == NULL);
}

List::~List() {
	while (head)
	{
//		Tail = head->next;
		delete head;
//		head = Tail;
	}
}

int main()
{
    return 0;
}

