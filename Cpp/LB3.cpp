// LB3.cpp: определяет точку входа для консольного приложения.
//

#include <iostream>

using namespace std;

struct Node {
	int value;
	Node *next;
};

class List {
	Node *head;
	Node *C;
	Node *O;
public:
	List() :head(NULL),C(NULL),O(NULL) {} 
	~List();

	void add(int);
	void show();
	bool isEmpty();
	void sortChoise();
	void sortBuble();
	void addSorted(int);
};

void List::add(int val) {
	Node *temp = new Node;                  
	temp->value = val;
	if (isEmpty()) temp->next = NULL; else temp->next = head;
	head = temp;
}

void List::show() {
	Node * temp;
	temp = head;
	while (temp) {
		cout << temp->value << ' ';
		temp = temp->next;
	}
	cout << endl;
}

bool List::isEmpty() {
	return (head == NULL);
}

List::~List() {
	Node *t = NULL;
	while (head)
	{
		t = head->next;
		delete head;
		head = t;
	}
	if (t) delete t;
}

void List::sortChoise() {
	O = NULL;

	while (O != head)
	{
		C = head;
		Node *prevMax = NULL;
		Node *Max = head;
		while (C->next != O)
		{
			Node *t = C;
			C = C->next;
			if (C->value > Max->value) {
				prevMax = t; Max = C;
			}
		}
		if (C != Max) {
			if (prevMax) prevMax->next = Max->next; else head = Max->next; 
			C->next = Max;
			Max->next = O;
			O = Max;
		} else  O = C;
	}
}

void List::sortBuble() {
	O = NULL;
	Node *P;
	while (O != head) {
		P = NULL;
		C = head;
		while (C->next != O) {
			if(C->value > C->next->value) {
				Node *t = C->next->next;
				C->next->next = C;
				C->next = t;
				if (P) P->next = C->next; else head = C->next;
			}
			P = C;
			C = C->next;
		}
		O = C;
	}
}

void List::addSorted(int val) {

}

int main()
{
	List lst1;

	cout << "---------------------------------------"<< "Choise" << "---------------------------------------" << endl;
	lst1.add(1);
	lst1.add(3);
	lst1.add(2);
	lst1.add(4);
	lst1.add(5);
	lst1.show();

	cout << "\n Sorted:"<<endl;
	lst1.sortChoise();
	lst1.show();

	cout << "\n ---------------------------------------" << " Bubble" << "---------------------------------------" << endl;


	system("pause");
    return 0;
}

