// ConsoleApplication2.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <cstring>
#include <algorithm>

using namespace std;

struct associate
{
	char cipher[4], f[20], i[20], o[20];    
	int yy, edu;                            

	void in(istream &);                    
	void out(ostream &);                    
};

void associate::in(istream & in)
{
	in >> cipher >> f >> i >> o >> yy >> edu;
}

void associate::out(ostream & out)
{
	out << cipher << ' ' << f << ' ' << i << ' ' << o << ' ' << yy << ' ' << edu << '\n';
}

bool compare(associate a, associate b) {    
	return a.edu == b.edu ? a.yy > b.yy : a.edu < b.edu;
}

int main()
{
	ifstream fin("input.txt");					///Открыли файл для чтения
	int n;
	char cipher[4];
	fin.getline(cipher, 4);						///Читаем строку из файла
	fin >> n;
	associate *q = new associate[n];           ///Создаем массив сотрудников

	for (associate *i = q; i < q + n; i++) {
		(*i).in(fin);
	}

	int k = 0;
	for (associate *i = q; i < q + n; i++)      ///Считаем количество сотрудников с нужным шифром
		if (!strcmp((*i).cipher, cipher))
			k++;

	associate **m = new associate *[k];         ///Создаем массив указателей на
	k = 0;                                      ///Сотрудников
	for (associate *i = q; i < q + n; i++)      ///С
		if (!strcmp((*i).cipher, cipher)) {       ///Нужным
			m[k] = i;                           ///Шифром
			k++;
		}

	sort(*m, *m + k, compare);                  

	for (associate *i = q; i < q + n; i++) {    
		(*i).out(cout);
	}

	fin.close();        ///Закрыли файлы и очистили память
	delete[] q;
	delete[] m;
	system("pause");
	return 0;
}

