#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;

void showIfPalindrome(std::string inputString, int i, int j)
{
	int candidateLength = j - i + 1;
	bool isPalindrome = true;
	for (int k = 0; k < candidateLength / 2; k++)
	{
		if (inputString[i + k] != inputString[j - k])
		{
			isPalindrome = false;
			break;
		}
	}

	if (isPalindrome)
	{
		std::string palindrome = inputString.substr(i, candidateLength);
		cout << i << "-" << j << ": " << palindrome << "\n";
	}
}

void findAndShowPalindromes(std::string inputString)
{
	int length = inputString.length();

	for (int i = 0; i < length - 1; i++)
	{
		for (int j = i + 1; j < length; j++)
		{
			showIfPalindrome(inputString, i, j);
		}
	}
}

void findAndShowPalindromesRecurrentInternal(std::string inputString, int length, int i, int j)
{
	if (j == length)
	{
		i++;
		if (i == length - 1)
		{
			return;
		}

		j = i + 1;
	}
	
	showIfPalindrome(inputString, i, j);

	findAndShowPalindromesRecurrentInternal(inputString, length, i, j + 1);
}

void findAndShowPalindromesRecurrent(std::string inputString)
{
	int length = inputString.length();
	findAndShowPalindromesRecurrentInternal(inputString, length, 0, 1);
}

int main()
{
	std::string inputString = "abdfegenhneg87r779";

	cout << "Linear search" << "\n\n";
	findAndShowPalindromes(inputString);

	cout << "\n" << "Recurrent search" << "\n\n";
	findAndShowPalindromesRecurrent(inputString);

	cout << "\n";

	system("pause");
	return 0;
}