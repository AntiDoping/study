#include <iostream>
#include <string>
#include "Train.h"
#include <iomanip>

Train::Train() {}

Train::Train(string destination, string number, int commonSeatsCount, int compartmentSeatsCount, int reservedSeatsCount, Time departureTime)
{
	_destination = destination;
	_number = number;
	_commonSeatsCount = commonSeatsCount;
	_compartmentSeatsCount = compartmentSeatsCount;
	_reservedSeatsCount = reservedSeatsCount;
	_departureTime = departureTime;
}

string Train::getDestination()
{
	return _destination;
}

string Train::getNumber()
{
	return _number;
}

int Train::getCommonSeatsCount()
{
	return _commonSeatsCount;
}

int Train::getCompartmentSeatsCount()
{
	return _compartmentSeatsCount;
}

int Train::getReservedSeatsCount()
{
	return _reservedSeatsCount;
}

void Train::setDepartureTime(Time departureTime)
{
	_departureTime = departureTime;
}

Time Train::getDepartureTime()
{
	return _departureTime;
}

void Train::Show()
{
	cout << "Destination:             " << _destination << endl;
	cout << "Number:                  " << _number << endl;
	cout << "Departure Time:          " << (int)_departureTime.Hour << ":" << setfill('0') << setw(2) << (int)_departureTime.Minute << endl;
	cout << setfill("") << setw(0);
	cout << "Common Seats Count:      " << _commonSeatsCount << endl;
	cout << "Compartment Seats Count: " << _compartmentSeatsCount << endl;
	cout << "Reserved Seats Count:    " << _reservedSeatsCount << endl;
}