#include <iostream>
#include "Train.h"
#include <conio.h>

Train inputTrain()
{
	cout << "Input destination: ";
	string destination;
	cin >> destination;

	cout << "Input number: ";
	string number;
	cin >> number;

	cout << "Input common seats count: ";
	int commonSeatsCount;
	cin >> commonSeatsCount;

	cout << "Input compartment seats count: ";
	int compartmentSeatsCount;
	cin >> compartmentSeatsCount;

	cout << "Input reserved seats count: ";
	int reservedSeatsCount;
	cin >> reservedSeatsCount;

	cout << "Input departureTime hours: ";
	int departureTimeHour;
	cin >> departureTimeHour;

	cout << ", minutes: ";
	int departureTimeMinute;
	cin >> departureTimeMinute;

	Time departureTime = { (unsigned char)departureTimeHour, (unsigned char)departureTimeMinute };
	return Train(destination, number, commonSeatsCount, compartmentSeatsCount, reservedSeatsCount, departureTime);
}


void ShowTrain(Train train, int index)
{
	cout << "================" << endl;
	cout << "Train #" << index + 1 << endl;
	cout << "----------------" << endl;

	train.Show();
}

void showFilteredTrainsByDestinationAndSeats(Train* trains, int totalCount, string destination, int minCommonSeatsCount)
{
	for (int i = 0; i < totalCount; i++)
	{
		Train train = trains[i];

		if (train.getDestination() == destination && train.getCommonSeatsCount() >= minCommonSeatsCount)
		{
			ShowTrain(train, i);
		}
	}
}

void showFilteredTrainsByDestinationAndHour(Train* trains, int totalCount, string destination, int hour)
{
	for (int i = 0; i < totalCount; i++)
	{
		Train train = trains[i];
		Time departureTime = train.getDepartureTime();

		if (train.getDestination() == destination && departureTime.Hour >= hour)
		{
			ShowTrain(train, i);
		}
	}
}

int main()
{
	// Debug version of input trains data
	/**
	int trainsCount = 3;
	Train trains[]
	{
		{"Minsk", "N1", 11, 12, 13, { 0, 10 }},
		{"Minsk", "N2", 0, 22, 23, { 13, 50 }},
		{"Grodno", "N3", 31, 32, 33, { 16, 40 }},
	};/**/

	// Release version of input trains data
	/**/
	cout << "Input trains count: ";
	int trainsCount;
	cin >> trainsCount;

	Train *trains = new Train[trainsCount];

	for (int i = 0; i < trainsCount; i++)
	{
		cout << "================" << endl;
		cout << "Train #" << i + 1 << " data input" << endl;
		cout << "----------------" << endl;

		Train train = inputTrain();
		trains[i] = train;
	}/**/

	cout << "*****************" << endl;
	cout << "Input destination: ";
	string destination;
	cin >> destination;
	cout << "----------------" << endl;
	cout << "Found trains with such destination" << endl << endl;
	showFilteredTrainsByDestinationAndSeats(trains, trainsCount, destination, 0);

	cout << "*****************" << endl;
	cout << "Input time hours: ";
	int hour;
	cin >> hour;
	cout << "----------------" << endl;
	cout << "Found trains with such destination and departure hour after specified" << endl << endl;
	showFilteredTrainsByDestinationAndHour(trains, trainsCount, destination, hour);

	cout << "----------------" << endl;
	cout << "Found trains with such destination and having common seats" << endl << endl;
	showFilteredTrainsByDestinationAndSeats(trains, trainsCount, destination, 1);

	cout << endl << "*****************" << endl;
	cout << "The end of the program." << endl;

	system("pause");

	return 0;
}