#include <string>
#include "Time.h"

using namespace std;

class Train
{
private:
	string _destination;
	string _number;
	int _commonSeatsCount;
	int _compartmentSeatsCount;
	int _reservedSeatsCount;
	Time _departureTime;

public:
	Train();
	Train(string destination, string number, int commonSeatsCount, int compartmentSeatsCount, int reservedSeatsCount, Time departureTime);

	string getDestination();
	string getNumber();
	int getCommonSeatsCount();
	int getCompartmentSeatsCount();
	int getReservedSeatsCount();

	void setDepartureTime(Time departureTime);
	Time getDepartureTime();

	void Show();
};