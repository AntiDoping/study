#include "stdafx.h"
#include <iostream>
#include <fstream>

using namespace std;

struct point
{
	int x;
	int y;

	void read(istream & stream)
	{
		stream >> x;
		stream.ignore(1);
		stream >> y;
	}

	void write(ostream & stream)
	{
		stream << x << ',' << y;
	}
};

struct triangle
{
	point p0;
	point p1;
	point p2;

	void read(istream & stream)
	{
		p0.read(stream);
		p1.read(stream);
		p2.read(stream);
	}

	void write(ostream & stream)
	{
		p0.write(stream);
		stream << ' ';
		p1.write(stream);
		stream << ' ';
		p2.write(stream);
		stream << '\n';
	}
};

struct triangleListItem
{
	triangle triangle;
	triangleListItem* next;
};

class trianglesList
{
private:
	triangleListItem* first;
	triangleListItem* current;
	triangleListItem* last;

public:
	void reset()
	{
		current = NULL;
	}

	bool moveNext()
	{
		current = current == NULL ? first : (*current).next;
		return current != NULL;
	}

	triangle getCurrent()
	{
		return (*current).triangle;
	}

	void insertAfter(triangle newTriangle)
	{
		if (current == NULL)
			throw;

		triangleListItem* newListItem = new triangleListItem();
		(*newListItem).triangle = newTriangle;

		triangleListItem* next = (*current).next;
		(*current).next = newListItem;
		(*newListItem).next = next;

		if (last == current)
		{
			last = newListItem;
		}
	}

	void append(triangle newTriangle)
	{
		triangleListItem* newListItem = new triangleListItem();
		(*newListItem).triangle = newTriangle;

		if (first == NULL)
		{
			first = newListItem;
			last = newListItem;
		}
		else
		{
			(*last).next = newListItem;
			last = newListItem;
		}
	}

	~trianglesList()
	{
		if (first != NULL)
		{
			triangleListItem* current = first;
			triangleListItem* next;
			while (current != NULL)
			{
				next = (*current).next;
				delete current;
				current = next;
			}

			first = NULL;
		}
	}
};

trianglesList* loadList(const char *filename)
{
	ifstream inputStream(filename);

	trianglesList* result = new trianglesList();
	triangle t;
	string line;
	int count;
	inputStream >> count;
	for (int i = 0; i < count; i++)
	{
		t.read(inputStream);
		(*result).append(t);
	}

	return result;
};

void printTriangles(trianglesList* list)
{
	(*list).reset();
	while ((*list).moveNext())
	{
		triangle current = (*list).getCurrent();
		current.write(cout);
	}
}

int getQuadrantFlag(point p)
{
	if (p.x >= 0 && p.y >= 0)
		return 1;
	
	if (p.x < 0 && p.y >= 0)
		return 2;

	if (p.x < 0 && p.y < 0)
		return 4;

	if (p.x >= 0 && p.y < 0)
		return 8;
}

bool isMagicTriangle(triangle t)
{
	int q0 = getQuadrantFlag(t.p0);
	int q1 = getQuadrantFlag(t.p1);
	int q2 = getQuadrantFlag(t.p2);

	int sum = q0 | q1 | q2;

	return (sum == 7) || (sum == 14) || (sum == 13) || (sum == 11);
}

point mirrorVertically(point p)
{
	point result;

	result.x = -p.x;
	result.y = p.y;

	return result;
}

triangle createMirroredMagicTriangle(triangle t)
{
	triangle result;

	result.p0 = mirrorVertically(t.p0);
	result.p1 = mirrorVertically(t.p1);
	result.p2 = mirrorVertically(t.p2);

	return result;
}

void insertMirroredMagicTriangles(trianglesList* list)
{
	(*list).reset();
	while ((*list).moveNext())
	{
		triangle t = (*list).getCurrent();

		if (isMagicTriangle(t))
		{
			triangle mirrored = createMirroredMagicTriangle(t);
			(*list).insertAfter(mirrored);
			(*list).moveNext();
		}
	}
}

int main()
{
	trianglesList* list = loadList("input.txt");

	cout << "Before:\n\n";
	printTriangles(list);

	insertMirroredMagicTriangles(list);

	cout << "\nAfter:\n\n";
	printTriangles(list);

	cout << "\n";
	system("pause");

	delete list;

	return 0;
}