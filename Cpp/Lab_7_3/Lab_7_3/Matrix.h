#include <sstream>
#include <iostream>
using namespace std;

#include "Exceptions.h"

template <class T>
class Matrix
{
private:
	static int MatrixCount;
	T* _data;
	int _width;
	int _height;
	string name;

	void InitMatrix(T* data, int width, int height)
	{
		_data = data;
		_width = width;
		_height = height;
		MatrixCount++;

		stringstream ss;
		ss << "Matrix ";
		ss << MatrixCount;
		name = ss.str();
	}

	bool IsSizeEquals(const Matrix<T> &m)
	{
		return m._width == this->_width && m._height == this->_height;
	}

	void EnsureSizeEquals(const Matrix<T> &m)
	{
		if (!IsSizeEquals(m)) throw EXC_SIZE_DOES_NOT_MATCH;
	}

	void EnsureTransposeSizeEquals(const Matrix<T> &m)
	{
		if (!(m._height == this->_width && m._width == this->_height)) throw EXC_SIZE_DOES_NOT_MATCH;
	}

	void ShowRowInternal(T* addr, int width)
	{
		cout << "{ ";

		for (int x = 0;x < width; x++)
		{
			cout << *addr;
			addr++;

			if (x < width - 1) cout << ", ";
		}

		cout << " }";
	}

	void ShowInternal(int x0, int y0, int width, int height)
	{
		cout << name << " data [" << y0 << "-" << y0 + height - 1 << ", " << x0 << "-" << x0 + width - 1 << "]" << "\n";
		for (int y = 0; y < height; y++)
		{
			T* addr = _data + (y0 + y) * _width + x0;

			ShowRowInternal(addr, width);

			if (y < height - 1) cout << ", ";
			cout << "\n";
		}
	}

	void LoadRowInternal(T* addr, int width)
	{
		cout << "{ ";

		for (int x = 0;x < width; x++)
		{
			cin >> *addr;
			addr++;

			if (x < width - 1) cout << ", ";
		}

		cout << " }";
	}

	void LoadInternal(int x0, int y0, int width, int height)
	{
		cout << "Load " << name << " data [" << y0 << "-" << y0 + height - 1 << ", " << x0 << "-" << x0 + width - 1 << "]" << "\n";
		for (int y = 0; y < height; y++)
		{
			T* addr = _data + (y0 + y) * _width + x0;

			LoadRowInternal(addr, width);

			if (y < height - 1) cout << ", ";
			cout << "\n";
		}
	}

public:

	Matrix()
	{
		int width = 50;
		int height = 50;
		T* data = new T[width * height];
		InitMatrix(data, width, height);
	}

	Matrix(int width, int height)
	{
		T* data = new T[width * height];
		InitMatrix(data, width, height);
	}

	Matrix(T* data, int width, int height)
	{
		InitMatrix(data, width, height);
	}

	~Matrix()
	{
		//delete _data;
	}

	Matrix<T> operator + (const Matrix<T> &m)
	{
		EnsureSizeEquals(m);

		int size = _width * _height;
		T* resultData = new T[size];
		for (int i = 0; i < size; i++)
		{
			resultData[i] = _data[i] + m._data[i];
		}

		return Matrix<T>(resultData, _width, _height);
	}

	Matrix<T> operator - (const Matrix<T> &m)
	{
		EnsureSizeEquals(m);

		int size = _width * _height;
		T* resultData = new T[size];
		for (int i = 0; i < size; i++)
		{
			resultData[i] = _data[i] - m._data[i];
		}

		return Matrix<T>(resultData, _width, _height);
	}

	Matrix<T> operator * (const Matrix<T> &m)
	{
		EnsureTransposeSizeEquals(m);

		int rWidth = m._width;
		int rHeight = _height;
		int size = rWidth * rHeight;
		T* resultData = new T[size];

		for (int y = 0; y < rWidth; y++)
		{
			for (int x = 0; x < rHeight; x++)
			{
				T sum = 0;
				for (int i = 0; i < _width; i++)
				{
					sum += _data[y * _width + i] * m._data[i * _height + x];
				}

				resultData[y * rWidth + x] = sum;
			}
		}

		return Matrix<T>(resultData, rWidth, rHeight);
	}

	Matrix<T> operator ^ (const int &pow)
	{
		Matrix<T> result = *this;

		for (int i = 0; i < pow - 1; i++)
		{
			result = result * (*this);
		}
		
		return result;
	}

	Matrix<T> operator * (const T &v)
	{
		int size = _width * _height;
		T* resultData = new T[size];
		for (int i = 0; i < size; i++)
		{
			resultData[i] = _data[i] * v;
		}

		return Matrix<T>(resultData, _width, _height);
	}

	Matrix<T> operator / (const T &v)
	{
		int size = _width * _height;
		T* resultData = new T[size];
		for (int i = 0; i < size; i++)
		{
			resultData[i] = _data[i] / v;
		}

		return Matrix<T>(resultData, _width, _height);
	}

	Matrix<T> operator += (const T &v)
	{
		int size = _width * _height;
		for (int i = 0; i < size; i++)
		{
			_data[i] = _data[i] + v;
		}

		return *this;
	}

	Matrix<T> operator -= (const T &v)
	{
		int size = _width * _height;
		for (int i = 0; i < size; i++)
		{
			_data[i] = _data[i] - v;
		}

		return *this;
	}

	Matrix<T> operator *= (const T &v)
	{
		int size = _width * _height;
		for (int i = 0; i < size; i++)
		{
			_data[i] = _data[i] * v;
		}

		return *this;
	}

	Matrix<T> operator /= (const T &v)
	{
		int size = _width * _height;
		for (int i = 0; i < size; i++)
		{
			_data[i] = _data[i] / v;
		}

		return *this;
	}

	bool operator == (const Matrix<T> &m)
	{
		if (!IsSizeEquals(m)) return false;

		int size = _width * _height;
		for (int i = 0; i < size; i++)
		{
			if (_data[i] != m._data[i]) return false;
		}

		return true;
	}

	bool operator != (const Matrix<T> &m)
	{
		return !(*this == m);
	}

	T RowSum(int row)
	{
		if (row < 0 || row >= _height) throw EXC_INDEX_OUT_OF_RANGE;

		T sum = 0;
		T* addr = _data + row * _width;
		for (int i = 0; i < _width; i++)
		{
			sum += *addr;
			addr++;
		}

		return sum;
	}

	T ColSum(int column)
	{
		if (column < 0 || column >= _width) throw EXC_INDEX_OUT_OF_RANGE;

		T sum = 0;
		T* addr = _data + column;
		for (int i = 0; i < _height; i++)
		{
			sum += *addr;
			addr += _width;
		}

		return sum;
	}

	T Min()
	{
		int size = _width * _height;

		if (size == 0) throw EXC_NO_DATA;

		T min = _data[0];
		for (int i = 1; i < size; i++)
		{
			if (min > _data[i]) min = _data[i];
		}

		return min;
	}

	T Max()
	{
		int size = _width * _height;

		if (size == 0) throw EXC_NO_DATA;

		T max = _data[0];
		for (int i = 1; i < size; i++)
		{
			if (max < _data[i]) max = _data[i];
		}

		return max;
	}

	T* ColMin()
	{
		int size = _width * _height;

		if (size == 0) throw EXC_NO_DATA;

		T* colMin = new T[_width];

		for (int x = 0; x < _width; x++)
		{
			colMin[x] = _data[x];
			for (int y = 1; y < _height; y++)
			{
				T v = _data[y * _width + x];
				if (colMin[x] > v) colMin[x] = v;
			}
		}

		return colMin;
	}

	void Show()
	{
		ShowInternal(0, 0, _width, _height);
	}

	void Show(int x0, int y0, int width, int height)
	{
		ShowInternal(x0, y0, width, height);
	}

	void ShowRow(T* data, int width)
	{
		ShowRowInternal(data, width);
	}

	void Load(int x0, int y0, int width, int height)
	{
		LoadInternal(x0, y0, width, height);
	}
};

template <class T>
int Matrix<T>::MatrixCount = 0;