#include <iostream>
using namespace std;
#include "Matrix.h"

const int MatrixWidth = 3;
const int MatrixHeight = 2;

int main()
{
	int matrixSize = MatrixWidth * MatrixHeight;

	cout << "\n**********************************\n";
	double m1Data[] = { 1, 2, 3, 9, 8, 7 };
	Matrix<double> m1 = Matrix<double>(m1Data, MatrixWidth, MatrixHeight);
	cout << "m1 = ";
	m1.Show();

	cout << "\n**********************************\n";
	double m2Data[] = { 10, 20, 30, 90, 80, 70 };
	Matrix<double> m2 = Matrix<double>(m2Data, MatrixWidth, MatrixHeight);
	cout << "m2 = ";
	m2.Show();

	cout << "\n**********************************\n";
	cout << "m2 + m1 = ";
	(m2 + m1).Show();

	cout << "\n**********************************\n";
	cout << "m2 - m1 = ";
	(m2 - m1).Show();

	cout << "\n**********************************\n";
	cout << "m1 * 3 = ";
	(m1 * 3).Show();

	cout << "\n**********************************\n";
	cout << "m2 / 10 = ";
	Matrix<double> m6 = m2 / 10;
	m6.Show();

	cout << "\n**********************************\n";
	cout << "(m1 == m6) = ";
	cout << (m1 == m6);

	cout << "\n**********************************\n";
	cout << "(m1 != m6) = ";
	cout << (m1 != m6);

	cout << "\n**********************************\n";
	cout << "(m1 += 1) = ";
	m1 += 1;
	m1.Show();

	cout << "\n**********************************\n";
	cout << "(m1 -= 1) = ";
	m1 -= 1;
	m1.Show();

	cout << "\n**********************************\n";
	cout << "(m1 *= 3) = ";
	m1 *= 3;
	m1.Show();

	cout << "\n**********************************\n";
	cout << "(m1 /= 3) = ";
	m1 /= 3;
	m1.Show();

	cout << "\n**********************************\n";
	cout << "m1.RowSum(1) = ";
	cout << m1.RowSum(1);
	cout << "\n";

	cout << "\n**********************************\n";
	cout << "m1.ColSum(1) = ";
	cout << m1.ColSum(1);
	cout << "\n";

	cout << "\n**********************************\n";
	cout << "m1.Min() = ";
	cout << m1.Min();
	cout << "\n";

	cout << "\n**********************************\n";
	cout << "m1.Max() = ";
	cout << m1.Max();
	cout << "\n";

	cout << "\n**********************************\n";
	cout << "m1.ColMin() = ";
	double* colMin = m1.ColMin();
	m1.ShowRow(colMin, MatrixWidth);
	cout << "\n";

	cout << "\n**********************************\n";
	double m8Data[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	Matrix<double> m8 = Matrix<double>(m8Data, 3, 3);
	m8.Show();
	cout << "\n";
	m8.Show(1, 0, 2, 2);

	cout << "\n**********************************\n";
	//m1.Load(1, 0, 2, 2);
	cout << "\n";
	m1.Show();

	cout << "\n**********************************\n";
	double aData[] = { 1, 2, 4, 2, 0, 3 };
	Matrix<double> a = Matrix<double>(aData, MatrixWidth, MatrixHeight);
	cout << "a = ";
	a.Show();

	cout << "\n";
	double bData[] = { 2, 5, 1, 3, 1, 1 };
	Matrix<double> b = Matrix<double>(bData, MatrixHeight, MatrixWidth);
	cout << "b = ";
	b.Show();

	cout << "\n";
	cout << "a x b = ";
	Matrix<double> c = a * b;
	c.Show();

	cout << "\n**********************************\n";
	double dData[] = { 1, 3, 5, 7 };
	Matrix<double> d = Matrix<double>(dData, 2, 2);
	cout << "d = ";
	d.Show();

	cout << "\n";
	cout << "d ^ 3 = ";
	(d ^ 3).Show();
}