#include "stdafx.h"
#include <iostream>

using namespace std;

const float e1 = 1e-5;
const float e2 = 1e-6;
int kFunc = 1;

float myExp(float x)
{
	float xx = 1;
	float factorial = 1;
	float sum = 1;
	float d;
	int i = 1;

	do
	{
		xx = xx * x;
		factorial = factorial * i;

		d = xx / factorial;
		sum = sum + d;

		i++;
	}
	while (d >= e2);

	return sum;
}

// ������� ��� �������� ��������� � �������� ���������� �������

float funcTest(float x)
{
	return x * x * x - 5 * x * x + 2 * x + 8; // (x + 1) * (x - 2) * (x - 4), �����: -1, 2, 4
}

float funcTest_(float x)
{
	return 3 * x * x - 10 * x + 2;
}

// ������� ��������� � �������

float k;

float func(float x)
{
	return 3 * x - exp(k * x);
}

float func_(float x)
{
	return 3 - k * exp(k * x);
}

//

bool solve(float(*func)(float x), float(*func_)(float x), float xMin, float xMax, float* result)
{
	float x = (xMin + xMax) / 2;
	bool isRootFound;
	int i = 0;
	int maxIterations = 1 / e1;

	do
	{
		float d = func(x) / func_(x);
		x = x - d;
		isRootFound = fabs(d) < e1;

		i++;
	} while (!isRootFound && x >= xMin && x <= xMax && i < maxIterations);

	(*result) = x;

	return isRootFound;
}

float(*f)(float x);
float(*f_)(float x);

void calculateAndShow(float xMin, float xMax)
{
	cout << "Range [" << xMin << ", " << xMax << "], Result = ";

	float result;
	if (solve(f, f_, xMin, xMax, &result))
	{
		cout << result << "; ";
		cout << "func(" << result << ") = " << f(result) << "\n";
	}
	else
	{
		cout << "not found" << "\n";
	}
}

void calculateAndShowWithAdvancedArg(float xMin, float xMax, float argK)
{
	k = argK;
	cout << "k = " << k << ", ";
	calculateAndShow(xMin, xMax);
}

int main()
{
	// �������� �� �������� �������

	f = &funcTest;
	f_ = &funcTest_;

	cout << "func(x) = (x + 1) * (x - 2) * (x - 4)" << "\n\n";

	calculateAndShow(-10, 0.5);
	calculateAndShow(-0.5, 1.5);
	calculateAndShow(1, 3);
	calculateAndShow(2.5, 10);

	cout << "\n";

	// �������� �� ������� �� �������

	f = &func;
	f_ = &func_;

	cout << "func(x) = 3 * x - exp(k * x)" << "\n\n";

	calculateAndShowWithAdvancedArg(-10, 10, 1);
	calculateAndShowWithAdvancedArg(-10, 10, 2);
	calculateAndShowWithAdvancedArg(-10, 10, 3);
	calculateAndShowWithAdvancedArg(-10, 10, 4);
	calculateAndShowWithAdvancedArg(-10, 10, 5);


	cout << "\n";
	system("pause");
	return 0;
}