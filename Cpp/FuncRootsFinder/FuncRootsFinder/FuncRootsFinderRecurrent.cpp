#include "stdafx.h"
#include <iostream>

using namespace std;

const float e1 = 1e-5;
const float e2 = 1e-6;
int kFunc = 1;

float myExpRecurrent(float x, float xx, float factorial, int i, float sum)
{
	xx = xx * x;
	factorial = factorial * i;

	float d = xx / factorial;
	sum = sum + d;

	if (d < e2)
	{
		return sum;
	}
	else
	{
		return myExpRecurrent(x, xx, factorial, i + 1, sum);
	}
}

float myExp(float x)
{
	float xx = 1;
	float factorial = 1;
	float sum = 1;
	int i = 1;
	return myExpRecurrent(x, xx, factorial, i, sum);// ���������� ��� ������������ ������������� ��� �����������
}

// ������� ��� �������� ��������� � �������� ���������� �������

float funcTest(float x)
{
	return x * x * x - 5 * x * x + 2 * x + 8; // (x + 1) * (x - 2) * (x - 4), �����: -1, 2, 4
}

float funcTest_(float x)
{
	return 3 * x * x - 10 * x + 2;
}

// ������� ��������� � �������

float k;

float func(float x)
{
	return 3 * x - exp(k * x);
}

float func_(float x)
{
	return 3 - k * exp(k * x);
}

//

bool solveRecurrent(float(*func)(float x), float(*func_)(float x), float xMin, float xMax, float x, int iterationsLeft, float* result)
{
	float d = func(x) / func_(x);
	x = x - d;
	bool isRootFound = fabs(d) < e1;
	if (isRootFound)
	{
		(*result) = x;
		return true;
	}

	if (x < xMin || x > xMax || iterationsLeft == 0)
	{
		return false;
	}

	return solveRecurrent(func, func_, xMin, xMax, x, iterationsLeft - 1, result);
}

bool solve(float(*func)(float x), float(*func_)(float x), float xMin, float xMax, float* result)
{
	int maxIterations = 1000; // 1 / e1 ��� �� ��������, �.�. ��� ����� e1 � ������������ ������ ���� �������������
	float x = (xMin + xMax) / 2;
	return solveRecurrent(func, func_, xMin, xMax, x, maxIterations, result);// ���������� ��� ������������ ������������� ��� �����������
}

float(*f)(float x);
float(*f_)(float x);

void calculateAndShow(float xMin, float xMax)
{
	cout << "Range [" << xMin << ", " << xMax << "], Result = ";

	float result;
	if (solve(f, f_, xMin, xMax, &result))
	{
		cout << result << "; ";
		cout << "func(" << result << ") = " << f(result) << "\n";
	}
	else
	{
		cout << "not found" << "\n";
	}
}

void calculateAndShowWithAdvancedArg(float xMin, float xMax, float argK)
{
	k = argK;
	cout << "k = " << k << ", ";
	calculateAndShow(-10, 10);
}

int main()
{
	// �������� �� �������� �������

	f = &funcTest;
	f_ = &funcTest_;

	cout << "func(x) = (x + 1) * (x - 2) * (x - 4)" << "\n\n";

	calculateAndShow(-10, 0.5);
	calculateAndShow(-0.5, 1.5);
	calculateAndShow(1, 3);
	calculateAndShow(2.5, 10);

	cout << "\n";

	// �������� �� ������� �� �������

	f = &func;
	f_ = &func_;

	cout << "func(x) = 3 * x - exp(k * x)" << "\n\n";

	calculateAndShowWithAdvancedArg(-10, 10, 1);
	calculateAndShowWithAdvancedArg(-10, 10, 2);
	calculateAndShowWithAdvancedArg(-10, 10, 3);
	calculateAndShowWithAdvancedArg(-10, 10, 4);
	calculateAndShowWithAdvancedArg(-10, 10, 5);


	cout << "\n";
	system("pause");
	return 0;
}