#include <sstream>
#include <iostream>
using namespace std;

#include "VectorF.h"
#include "Exceptions.h"

void internalMain()
{
	float v1Items[] = { 1, 2, 4 };
	VectorF v1 = VectorF(v1Items, 3);

	float v2Items[] = { 4, 8, 16 };
	VectorF v2 = VectorF(v2Items, 3);

	VectorF v3 = v1 + v2;

	cout << "\n";
	v1.Show();
	cout << " + ";
	v2.Show();
	cout << " = ";
	v3.Show();


	VectorF v4 = v2 - v1;

	cout << "\n";
	v2.Show();
	cout << " - ";
	v1.Show();
	cout << " = ";
	v4.Show();


	VectorF v5 = v1 * 2;

	cout << "\n";
	v1.Show();
	cout << " * ";
	cout << "2";
	cout << " = ";
	v5.Show();


	VectorF v6 = v2 / 2;

	cout << "\n";
	v2.Show();
	cout << " / ";
	cout << "2";
	cout << " = ";
	v6.Show();


	VectorF v7 = v1 * v2;

	cout << "\n";
	v1.Show();
	cout << " * ";
	v2.Show();
	cout << " = ";
	v7.Show();


	VectorF v8 = v2 / v1;

	cout << "\n";
	v2.Show();
	cout << " / ";
	v1.Show();
	cout << " = ";
	v8.Show();

	
	cout << ", [2] = ";
	v8.Show(2);

	cout << ", [3] = ";
	v8.Show(3);

	cout << "\n";
}

string GetErrorMessage(int e)
{
	switch (e)
	{
		case EXC_INDEX_OUT_OF_RANGE:
			return "Index out of range.";
		case EXC_LENGTH_DOES_NOT_MATCH:
			return "Length does not match.";
		default:
		{
			return "-----";
			stringstream ss;
			ss << e;
			string str = ss.str();
			return "Unknow error code " + str + ".";
		}
	}
}

int main()
{
	try
	{
		internalMain();
	}
	catch (int e)
	{
		string message = GetErrorMessage(e);
		cout << "An error occured: " << message;
	}
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
