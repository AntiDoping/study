class VectorF {
private:
	float* _items;
	int _length;

	void InternalShow(int i);
	void ValidateIndex(int i);
	void EnsureLengthEquals(const VectorF &v);

public:
	VectorF(float* items, int length);
	~VectorF();

	float& operator[](int i);
	VectorF operator + (const VectorF &v);
	VectorF operator - (const VectorF &v);
	VectorF operator * (const VectorF &v);
	VectorF operator / (const VectorF &v);
	VectorF operator * (const float &v);
	VectorF operator / (const float &v);

	void Show();
	void Show(int i);
};