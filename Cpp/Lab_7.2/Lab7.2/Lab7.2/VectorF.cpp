#include <iostream>
using namespace std;

#include "VectorF.h"
#include "Exceptions.h"


VectorF::VectorF(float* items, int length)
{
	_items = items;
	_length = length;
}

VectorF::~VectorF()
{
	// DEBUG: � ���� ����� �����-��
	//delete[_length] _items;
}


float& VectorF::operator[](int i)
{
	ValidateIndex(i);
	return _items[i];
}

VectorF VectorF::operator + (const VectorF &v)
{
	EnsureLengthEquals(v);

	float* resultItems = new float[_length];
	for (int i = 0; i < _length; i++)
	{
		resultItems[i] = _items[i] + v._items[i];
	}

	return VectorF(resultItems, _length);
}

VectorF VectorF::operator - (const VectorF &v)
{
	EnsureLengthEquals(v);

	float* resultItems = new float[_length];
	for (int i = 0; i < _length; i++)
	{
		resultItems[i] = _items[i] - v._items[i];
	}

	return VectorF(resultItems, _length);
}

VectorF VectorF::operator * (const VectorF &v)
{
	EnsureLengthEquals(v);

	float* resultItems = new float[_length];
	for (int i = 0; i < _length; i++)
	{
		resultItems[i] = _items[i] * v._items[i];
	}

	return VectorF(resultItems, _length);
}

VectorF VectorF::operator / (const VectorF &v)
{
	EnsureLengthEquals(v);

	float* resultItems = new float[_length];
	for (int i = 0; i < _length; i++)
	{
		resultItems[i] = _items[i] / v._items[i];
	}

	return VectorF(resultItems, _length);
}

VectorF VectorF::operator * (const float &v)
{
	float* resultItems = new float[_length];
	for (int i = 0; i < _length; i++)
	{
		resultItems[i] = _items[i] * v;
	}

	return VectorF(resultItems, _length);
}

VectorF VectorF::operator / (const float &v)
{
	float* resultItems = new float[_length];
	for (int i = 0; i < _length; i++)
	{
		resultItems[i] = _items[i] / v;
	}

	return VectorF(resultItems, _length);
}


void VectorF::Show()
{
	cout << "(";
	int lastItem = _length - 1;
	for (int i = 0; i < lastItem; i++)
	{
		InternalShow(i);
		cout << ", ";
	}
	InternalShow(lastItem);

	cout << ")";
}

void VectorF::Show(int i)
{
	ValidateIndex(i);
	InternalShow(i);
}

void VectorF::InternalShow(int i)
{
	cout << _items[i];
}

void VectorF::ValidateIndex(int i)
{
	if (i < 0 || i >= _length)
		throw EXC_INDEX_OUT_OF_RANGE;
}

void VectorF::EnsureLengthEquals(const VectorF &v)
{
	if (v._length != this->_length)
		throw EXC_LENGTH_DOES_NOT_MATCH;
}