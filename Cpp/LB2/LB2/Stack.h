#pragma once
#include <iostream>
#include "Stack.h"

using namespace std;

class Stack {
	bool _isFixedSize;
	int _size;
	int _count;
	int *items;
public:
	Stack(int size);
	void push(int item);
	int pop();
	void removeOdd();
	void showItems();
	~Stack();
	void operator+(int item);
	int operator[](int n);

};

void remove(Stack *stacks, int count);