#include "Stack.h"
#include <iostream>



Stack::Stack(int size) {
	_isFixedSize = size > 0;
	_size = _isFixedSize ? size : 4;
	_count = 0;
	items = new int[_size];
}

void Stack::push(int item) {
	if (_count == _size) {
		if (_isFixedSize)
			return;
		else {
			int *newitems = new int[2 * _size];
			for (int i = 0; i < _size; i++)
				*(newitems + i) = *(items + i);
			_size *= 2;
			delete items;
			items = newitems;
		}
	}
	*(items + _count) = item;
	_count++;
}

int Stack::pop() {
	_count--;
	int t;
	t = *(items + _count);
	return t;
}

void Stack::removeOdd() {
	int offset = _count % 2;
	_count = _count / 2;
	for (int i = 0; i < _count; i++)
		*(items + i) = *(items + (i * 2) + offset);
}

void Stack::showItems() {
	cout << _size<< ":  ";
	for (int i = 0; i < _count; i++)
		cout << *(items + i) << " ";
}

Stack::~Stack() {
	delete items;
}

 
void Stack::operator+(int item) {
	if (_count == _size) {
		if (_isFixedSize)
			return;
		else {
			int *newitems = new int[2 * _size];
			for (int i = 0; i < _size; i++)
				*(newitems + i) = *(items + i);
			_size *= 2;
			delete items;
			items = newitems;
		}
	}
	*(items + _count) = item;
	_count++;
}

int Stack::operator[](int n) {
	int t;
	for (int i = 0; i < n; i++) {
		_count--;
		t = *(items + _count);
	}
	return t;
}


//------------------------------------------------------------------------------


void remove(Stack *stacks, int count) {
	for (int i = 0; i < count; i++)
		stacks[i].removeOdd();
}
