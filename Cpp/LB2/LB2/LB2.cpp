// LB2.cpp: определяет точку входа для консольного приложения.
//

#include <iostream>
#include "Stack.h"


int main()
{
	int count = 3;
	Stack stacks[] {
		{0}, {3}, {2}
	};
	cout << " Stack #1 \n";
	stacks[0]+1;
	stacks[0]+2;
	stacks[0]+3;
	stacks[0]+4;
	stacks[0]+5;
	stacks[0]+6;
	stacks[0].showItems();

	cout << "\n -------------------------------------------------------------";
	cout << "\n Stack #2 \n";

	stacks[1].push(10);
	stacks[1].push(20);
	stacks[1].push(30);
	stacks[1].push(40);
	stacks[1].showItems();

	cout << "\n -------------------------------------------------------------";
	cout << "\n Stack #3 \n";

	stacks[2].push(11);
	stacks[2].push(21);
	stacks[2].push(31);
	stacks[2].push(41);
	stacks[2].showItems();


	cout << "\n \n ==================================================================";
	cout << "\n After removing odd elements: \n";

	remove(stacks, count);
	for (int i = 0; i < count; i++) {
		cout << "\n Stack#" << i+1 << ": \n";
		stacks[i].showItems();
		cout << "\n";
	}

	cout << endl;
	stacks[0][2];
	stacks[0].showItems();

	system("pause");
	return 0;
}

